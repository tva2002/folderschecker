﻿using System;
using System.Collections.Generic;
using System.IO;
using FoldersChecker;
using FoldersChecker.DB;
using Xunit;
using Moq;
using Serilog;

namespace FoldersCheckerUnitTest
{
    public class CompareFileTests : IClassFixture<FilesFixture>
    {
        readonly FilesFixture fixture;

        public CompareFileTests(FilesFixture fixture)
        {
            this.fixture = fixture;
        }

        [Theory]
        [MemberData(nameof(GetFileNamesForGetComparedPachTest))]
        public void GetComparedPach_ReturnsOfValidPathFileForCompare(string oldVersionFileName, string expectedNewFileName)
        {
            var mockedRepository = new Mock<IRepository>();
            var mockedLogger = new Mock<ILogger>();
            var fileCheck = new Chfiles(mockedRepository.Object, mockedLogger.Object);

            var result = fileCheck.GetComparedPath(oldVersionFileName);

            Assert.Equal(expectedNewFileName, result);
        }

        [Fact]
        public void CheckFileinFoldrs_AddsCorrectFilesToFilesDeleteList()
        {
            var mockedRepository = new Mock<IRepository>();
            mockedRepository.Setup(x => x.GetSubfolfersByPath(It.IsAny<string>()))
                .Returns((string s) => new Subfolfers
                {
                    Directories = s,
                    CreationTime = "2020-12-12 04:32:11",
                    LastWriteTime = "2020-12-24 05:43:22"
                });
            var expectedFilesDelete = new List<string> { FilesFixture.FileWithNewLinesAtTheEndOldVersion };

            var mockedLogger = new Mock<ILogger>();
            var fileCheck = new Chfiles(mockedRepository.Object, mockedLogger.Object);
            fileCheck.CheckFileinFoldrs(FilesFixture.FileWithNewLinesAtTheEndOldVersion);
            fileCheck.CheckFileinFoldrs(FilesFixture.ChangedFileOldVersion);
            fileCheck.CheckFileinFoldrs(FilesFixture.DeletedFileOldVersion);

            Assert.Equal(expectedFilesDelete, fileCheck.FilesDelete);
        }

        [Theory]
        [MemberData(nameof(GetFileLinesStartingWithOtherFileLinesTestData))]
        public void FileLinesStartWith_ReturnsTrueWhenFileLinesStartWithOterFileLines(
            IEnumerable<string> fileLines, IEnumerable<string> startWithFileLines)
        {
            var mockedLogger = new Mock<ILogger>();
            var fileCompare = new CompareFile(mockedLogger.Object);
            Assert.True(fileCompare.FileLinesStartWith(fileLines, startWithFileLines));
        }

        [Theory]
        [MemberData(nameof(GetFileLinesNotStartingWithOtherFileLinesTestData))]
        public void FileLinesStartWith_ReturnsFalseWhenFileLinesDoNotStartWithOterFileLines(
            IEnumerable<string> fileLines, IEnumerable<string> startWithFileLines)
        {
            var mockedLogger = new Mock<ILogger>();
            var fileCompare = new CompareFile(mockedLogger.Object);
            Assert.False(fileCompare.FileLinesStartWith(fileLines, startWithFileLines));
        }

        #region MemberData Methods

        public static IEnumerable<object[]> GetFileNamesForGetComparedPachTest()
        {
            return new List<object[]>
            {
                new object[]
                {
                    FilesFixture.FileWithNewLinesAtTheEndOldVersion,
                    FilesFixture.FileWithNewLinesAtTheEnd
                },
                new object[]
                {
                    FilesFixture.ChangedFileOldVersion,
                    FilesFixture.ChangedFile
                }
            };
        }

        public static IEnumerable<object[]> GetFileLinesStartingWithOtherFileLinesTestData()
        {
            return new List<object[]>
            {
                new object[]
                {
                    new List<string> // new file lines
                    {
                        "ESP35,20200504,060027,6705,6697",
                        "ESP35,20200504,060027,6697,6689",
                        "ESP35,20200504,060031,6695,6687",
                        "ESP35,20200504,060035,6694,6686",
                        "ESP35,20200504,060035,6692,6684",
                        "ESP35,20200504,060036,6693,6685",
                    },
                    new List<string> // .versions file lines
                    {
                        "ESP35,20200504,060027,6705,6697",
                        "ESP35,20200504,060027,6697,6689",
                        "ESP35,20200504,060031,6695,6687",
                        "ESP35,20200504,060035,6694,6686",
                        "ESP35,20200504,060035,6692,6684",
                        "ESP35,20200504,060036,6693,6685",

                    }
                },
                new object[]
                {
                    new List<string> // new file lines
                    {
                        "ESP35,20200504,060027,6705,6697",
                        "ESP35,20200504,060027,6697,6689",
                        "ESP35,20200504,060031,6695,6687",
                        "ESP35,20200504,060035,6694,6686",
                        "ESP35,20200504,060035,6692,6684",
                        "ESP35,20200504,060036,6693,6685",

                    },
                    new List<string> // .versions file lines
                    {
                        "ESP35,20200504,060027,6705,6697",
                        "ESP35,20200504,060027,6697,6689",
                        "ESP35,20200504,060031,6695,6687",

                    }
                }
            };
        }

        public static IEnumerable<object[]> GetFileLinesNotStartingWithOtherFileLinesTestData()
        {
            return new List<object[]>
            {
                new object[]
                {
                    new List<string> // new file lines
                    {
                        "line2",
                    },
                    new List<string> // .versions file lines
                    {
                        "line1",
                    }
                },
                new object[]
                {
                    new List<string> // new file lines
                    {
                        "line1",
                    },
                    new List<string> // .versions file lines
                    {
                        "line1",
                        "line2"
                    }
                },
                new object[]
                {
                    new List<string> // new file lines
                    {
                        "line1",
                        "line2",
                        "line3",
                        "line4"
                    },
                    new List<string> // .versions file lines
                    {
                        "line1",
                        "line0",
                        "line3",
                        "line4"
                    }
                }
            };
        }

        #endregion
    }
}

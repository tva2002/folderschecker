﻿using Serilog;
using System;
using System.Linq;
using FoldersChecker.DB;
using System.Net;
using System.Net.Mail;


namespace FoldersChecker
{
    public class Email
    {
        private readonly ILogger _logger;

        public Email(Serilog.ILogger logger)
        {
            _logger = logger ?? throw new NullReferenceException(nameof(logger));
        }

        public void Send(string subject, string body)
        {
            try
            {
                FolderContext db = new FolderContext();
                var mailcfg = db.EmailConfig.FirstOrDefault();
                if (mailcfg != null)
                {
                    using (SmtpClient smtp = new SmtpClient(mailcfg.SmtpServer, mailcfg.Port))
                    {
                        NetworkCredential creds = new NetworkCredential(mailcfg.UserName, mailcfg.Password);
                        smtp.Credentials = creds;
                        smtp.EnableSsl = true;
                        smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                        MailMessage msg = new MailMessage(mailcfg.From, mailcfg.To, subject, body);
                        smtp.Send(msg);
                    };
                }
                else
                {
                    _logger.Error("Cannot send an email report. No mailboxes configured in the database.");
                }
            }
            catch (Exception ex)
            {
                _logger.Error("Email configuration error: {0}", ex);
            }
        }
    }
}

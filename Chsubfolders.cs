﻿using System;
using System.Linq;
using System.IO;
using FoldersChecker.DB;
using System.Collections.Generic;
using Serilog;

namespace FoldersChecker
{
    public class Chsubfolders
    {
        private readonly ILogger _logger;

        public Chsubfolders(ILogger logger)
        {
            _logger = logger ?? throw new NullReferenceException(nameof(logger));
        }

        public List<string> FoldersinSubfolderRezult { get { return foldersinSubfolderRezult; } }
        public List<string> FilesDeleteinSubfolder { get { return filesDeleteinSubfolder; } }
        public List<string> FilesAddinSubfolder { get { return filesAddinSubfolder; } }
        public List<string> FilesRemovetoVersionSubfolder { get { return filesRemovetoVersionSubfolder; } }
        public List<int> TipeFilesRemovetoVersionSubfolder { get { return tipeFilesRemovetoVersionSubfolder; } }
        private List<string> filesDeleteinSubfolder = new List<string>();
        private List<string> filesAddinSubfolder = new List<string>();
        private List<string> foldersinSubfolderRezult = new List<string>();
        private List<string> filesRemovetoVersionSubfolder = new List<string>();
        private List<int> tipeFilesRemovetoVersionSubfolder = new List<int>();

        public void ClearResults()
        {
            FoldersinSubfolderRezult.Clear();
            FilesDeleteinSubfolder.Clear();
            FilesAddinSubfolder.Clear();
            FilesRemovetoVersionSubfolder.Clear();
            tipeFilesRemovetoVersionSubfolder.Clear();
        }

        private void GetListFilesRemovetoVersioninSubfolder(Chfiles filesDeleteinSubfolderlist)
        {
            foreach (var spath in filesDeleteinSubfolderlist.FilesRemovetoVersion)
            {
                filesRemovetoVersionSubfolder.Add(spath);
            }
        }

        private void GetTipeFilesRemovetoVersioninSubfolder(Chfiles filesTipeSubfolderlist)
        {
                        
                tipeFilesRemovetoVersionSubfolder.Add(filesTipeSubfolderlist.OnliZip);
            
        }

        private void GetListFilesDeleteinSubfolder(Chfiles filesDeleteinSubfolderlist)
        {
            foreach (var spath in filesDeleteinSubfolderlist.FilesDelete)
            {
                filesDeleteinSubfolder.Add(spath);
            }
        }

        private void GetListFilesAddinSubfolder(Chfiles filesAddinSubfolderlist)
        {

            foreach (var spath in filesAddinSubfolderlist.FilesRezult)
            {
                filesAddinSubfolder.Add(spath);
            }
        }

        private void GetListFoldersAddinSubfolder(Chfiles foldersAddinSubfolderlist)
        {

            foreach (var spath in foldersAddinSubfolderlist.FoldersRezult)
            {
                filesAddinSubfolder.Add(spath);
            }
        }

        public void checksubfolders(string subfolder)
        {
            Console.WriteLine(subfolder);
            IRepository repository = new Repository(new FolderContext());
            Chfiles CFiles = new Chfiles(repository, _logger);
            var filechek = repository.GetSubfolfersByPath(subfolder);
            if (filechek == null)
            {
                var subfolders = new Subfolfers
                {
                    Directories = subfolder,
                    CreationTime = Directory.GetCreationTime(subfolder).ToString(),
                    LastWriteTime = Directory.GetLastWriteTime(subfolder).ToString()
                };
                repository.AddSubfolders(subfolders);
                Directory
                    .GetFiles(subfolder, "*", SearchOption.TopDirectoryOnly)
                    .ToList()
                    .ForEach(fe => { CFiles.CheckFileinFoldrs(fe);
                        GetListFilesDeleteinSubfolder(CFiles);
                        GetListFilesAddinSubfolder(CFiles);
                        GetListFoldersAddinSubfolder(CFiles);
                        GetListFilesRemovetoVersioninSubfolder(CFiles);
                        GetTipeFilesRemovetoVersioninSubfolder(CFiles);
                        CFiles.ClearResults();
                    });                
                foldersinSubfolderRezult.Add(subfolder + " ADD folder - " + Directory.GetLastWriteTime(subfolder).ToString());
            }
            else
            {
                if (Directory.GetLastWriteTime(subfolder).ToString() != filechek.LastWriteTime)
                {
                    Directory
                             .GetFiles(subfolder, "*", SearchOption.AllDirectories)
                             .ToList()
                             .ForEach(fe => { CFiles.CheckFileinFoldrs(fe);
                                 GetListFilesDeleteinSubfolder(CFiles);
                                 GetListFilesRemovetoVersioninSubfolder(CFiles);
                                 GetListFoldersAddinSubfolder(CFiles);
                                 GetListFilesAddinSubfolder(CFiles);
                                 GetTipeFilesRemovetoVersioninSubfolder(CFiles);
                                 CFiles.ClearResults();
                             });
                    filechek.LastWriteTime = Directory.GetLastWriteTime(subfolder).ToString();
                    repository.SaveChanges();
                }
            }
        }

    }
}

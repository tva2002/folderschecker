﻿using System;
using System.Timers;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using FoldersChecker.DB;
using Serilog;
using System.Data.Entity;
using System.Configuration;

namespace FoldersChecker
{
    class Program
    {
        private static TimeSpan timerWaitTimeSpan = TimeSpan.FromHours(12);
        
        public static List<string> FilesRezult = new List<string>();
        public static List<string> FilesDelete = new List<string>();
        public static List<string> FilesRemovetoVersion = new List<string>();
        public static List<string> FoldersRezult = new List<string>();
        public static List<int> Tipefile = new List<int>();
        public static string mailBody = "The list of changes: \n\n";
        public static int OnliZipFile = 1;
        public static FolderContext db = new FolderContext();
        public static Timer timer = new Timer(timerWaitTimeSpan.TotalMilliseconds);

        static void clearFilesdb()
        {
            var file = db.Files;
            foreach (Files fl in file)
            {
                if ( File.Exists(fl.files)==false)                
               {
                    using (FolderContext Context = new FolderContext())
                     {
                       Files deptDelete = new Files { Id = fl.Id };
                      Context.Entry(deptDelete).State = EntityState.Deleted;
                      Context.SaveChanges();
                     }
                    FilesRezult.Add(fl.files + " - Deleted file");
                }
            }            
        }
       
        static void clearSubFoldersdb()
        {
            var SubFolders = db.Subfolfers;
            foreach (Subfolfers fl in SubFolders)
            {
                if (Directory.Exists(fl.Directories) == false)
                {
                    using (FolderContext Context = new FolderContext())
                    {
                        Subfolfers folderDelete = new Subfolfers { Id = fl.Id };
                        Context.Subfolfers.Attach(folderDelete);
                        Context.Subfolfers.Remove(folderDelete);
                        Context.SaveChanges();
                    }
                    FoldersRezult.Add(fl.Directories + " - Deleted folder");
                }
            }
        }

        private static void timer_Close(object sender, ElapsedEventArgs e)
        {
            timer.Close();
            timer.Dispose();
            Environment.Exit(0);
        }

        static void Main(string[] args)
        {
            Log.Logger = new LoggerConfiguration()
                .MinimumLevel.Debug()
                .WriteTo.Console()
                .WriteTo.File("logs\\FoldersChecker.log", rollingInterval: RollingInterval.Day, retainedFileCountLimit: 14)
                .CreateLogger();
            try
            {
                clearSubFoldersdb();
                clearFilesdb();
                IRepository repository = new Repository(db);
                Chfiles CFiles = new Chfiles(repository, Log.Logger);
                Chsubfolders CFolders = new Chsubfolders(Log.Logger);
                Email mail = new Email(Log.Logger);
                ZabbixAgent zabbixAgent = new ZabbixAgent(Log.Logger);
                var folders = db.Folders.ToList();
                string PathToMoveFiles = ConfigurationManager.AppSettings.Get("PathToMoveFiles");
                Directory.CreateDirectory(PathToMoveFiles);

                Log.Information("list files:");
                foreach (Folders folder in folders)
                {
                    Log.Debug("{0}.{1} -------------", folder.Id, folder.Directories);   
                    if (Directory.Exists(folder.Directories))
                    {
                        var mypath = folder.Directories + " - " + Directory.GetLastWriteTime(folder.Directories) + " - " + Directory.GetCreationTime(folder.Directories);
                        Log.Debug(mypath);
                        Directory
                            .GetDirectories(folder.Directories, "*", SearchOption.AllDirectories)
                            .ToList()
                            .ForEach(subfolder => { CFolders.checksubfolders(subfolder);
                                FilesDelete.AddRange(CFolders.FilesDeleteinSubfolder);
                                FoldersRezult.AddRange(CFolders.FoldersinSubfolderRezult);
                                FilesRezult.AddRange(CFolders.FilesAddinSubfolder);
                                FilesRemovetoVersion.AddRange(CFolders.FilesRemovetoVersionSubfolder);
                                Tipefile.AddRange(CFolders.TipeFilesRemovetoVersionSubfolder);
                                CFolders.ClearResults();
                            });
                             
                        Directory
                                .GetFiles(folder.Directories, "*", SearchOption.TopDirectoryOnly)
                                .ToList()
                                .ForEach(fe => { CFiles.CheckFileinFoldrs(fe);
                                    FilesDelete.AddRange(CFiles.FilesDelete);
                                    FilesRezult.AddRange(CFiles.FilesRezult);                                  
                                    FilesRemovetoVersion.AddRange(CFiles.FilesRemovetoVersion);
                                    Tipefile.Add(CFiles.OnliZip);
                                    CFiles.ClearResults();
                                });
                        if (Directory.GetLastWriteTime(folder.Directories).ToString() != folder.LastWriteTime)
                        {
                            FolderContext db1 = new FolderContext();
                            var customer = db1.Folders
                                            .Where(c => c.Directories == folder.Directories)
                                              .FirstOrDefault();
                            customer.LastWriteTime = Directory.GetLastWriteTime(folder.Directories).ToString();
                            db1.SaveChanges();
                            FoldersRezult.Add(folder.Directories + "\n");

                        }
                    }
                    else
                    {  
                        FoldersRezult.Add(folder.Directories + "- folder does not exist.\n");
                        Log.Debug(folder.Directories + "- folder does not exist.\n");
                    }
                }

                Log.Information("The list of changes (added or changed files and/or folders):");
                foreach (var item in FoldersRezult)
                {
                    mailBody += item + "\n\n";
                    Log.Debug(item);
                }
                foreach (var item in FilesRezult)
                {
                    mailBody += item + "\n\n";
                    Log.Debug(item);
                }

                foreach (var item in Tipefile)
                {
                    OnliZipFile = OnliZipFile * item ;
 //                   Log.Debug(item.ToString());
                }

 

                if (FilesRemovetoVersion.Count > 0)
                {
                    Log.Information("Deleted files and/or folders:");
                    foreach (var item in FilesRemovetoVersion)
                    {
                        mailBody += item + "\n\n";
                        Log.Debug(item);
                    }
                }

                if (FilesDelete.Count > 0)
                {
                    var msg1 = "List of files automatically moved to " + PathToMoveFiles;
                    var msg2 = "(these are updated files or zip-archives with changed last write time):";
                    Log.Information(msg1);
                    Log.Information(msg2);
                    mailBody += msg1 + "\n\n" + msg2 + "\n\n";

                    foreach (var path in FilesDelete)
                    {
                        try
                        {
                            File.Move(path, Path.Combine(PathToMoveFiles, Path.GetFileName(path)));
                            Log.Debug(path);
                            mailBody += path + "\n\n";
                        }
                        catch (Exception ex)
                        {
                            Log.Error(path + " - File move error " + ex.Message);
                            mailBody += path + " - File move error.\n\n";
                        }
                    } 
                }

                var subject = "Historical data backup. Daily report";


                foreach (var item in FilesRezult)
                {
                    if (item.Contains("Deleted"))
                    { OnliZipFile = OnliZipFile * 0; }

                }

                if ((FilesRezult.Count != 0 && OnliZipFile == 0) || (FilesRemovetoVersion.Count != 0 && OnliZipFile == 0) || ( FoldersRezult.Count !=0 && OnliZipFile == 0))
                {
                    zabbixAgent.Send("error");
                    Log.Debug("Status - error");
                    subject += " [ERROR]";
                    mailBody += "Status - error" + "\n\n";
                }
                else
                {
                    zabbixAgent.Send("OK");
                    Log.Debug("Status - OK");
                    subject += " [OK]";
                    mailBody += "Status - OK" + "\n\n";
                }

                mail.Send(subject, mailBody);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Something went wrong");
            }

            Log.Information($"Finished. This window will be closed automatically in {timerWaitTimeSpan} " +
                "or you can press any key to close it.");
            Log.CloseAndFlush();
            timer.Elapsed += timer_Close;
            timer.Start();
            Console.ReadKey();
        }
    }
}

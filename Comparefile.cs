﻿using Serilog;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;


namespace FoldersChecker
{
    public class CompareFile
    {
        private readonly ILogger _logger;

        public CompareFile(ILogger logger)
        {
            _logger = logger ?? throw new NullReferenceException(nameof(logger));
        }

        public bool FileLinesStartWith(IEnumerable<string> fileLines, IEnumerable<string> startWithFileLines)
        {
            IEnumerator FileLines = fileLines.GetEnumerator();
            IEnumerator StartWithFileLines = startWithFileLines.GetEnumerator();
            bool result = true;
            while (StartWithFileLines.MoveNext())
            {
                if (FileLines.MoveNext())
                {
                    if (FileLines.Current.ToString() != StartWithFileLines.Current.ToString())
                    {
                        result = false;
                        break;
                    }
                }
                else
                { 
                    result = false;
                    break;
                }
            }
            return result;
        }

        public bool ZipArchivesHaveSameContent(string versionsZipFile, string newSyncedZipFile)
        {
            using (var versionsZipArchive = ZipFile.OpenRead(versionsZipFile))
            using (var newSyncedZipArchive = ZipFile.OpenRead(newSyncedZipFile))
            {
                if (versionsZipArchive.Entries.Count != 1)
                {
                    _logger.Warning($"Archive '{versionsZipFile}' does not contain exactly one file.");
                    return false;
                }

                if (newSyncedZipArchive.Entries.Count != 1)
                {
                    _logger.Warning($"Archive '{newSyncedZipFile}' does not contain exactly one file.");
                    return false;
                }

                var versionEntry = versionsZipArchive.Entries[0];
                var newSyncedEntry = newSyncedZipArchive.Entries[0];
                if (versionEntry.FullName == newSyncedEntry.FullName && versionEntry.Length == newSyncedEntry.Length)
                {
                    using (var newSyncedEntryStreamReader = new StreamReader(newSyncedEntry.Open()))
                    using (var versionEntryStreamReader = new StreamReader(versionEntry.Open()))
                    {
                        if (FileLinesStartWith(ReadLines(newSyncedEntryStreamReader), ReadLines(versionEntryStreamReader)))
                        {
                            return true;
                        }
                    }
                }
            }

            return false;
        }

        private IEnumerable<string> ReadLines(StreamReader reader)
        {
            string line;
            while ((line = reader.ReadLine()) != null)
            {
                yield return line;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FoldersChecker.DB
{
    public class Repository : IRepository
    {
        private readonly FolderContext context;

        public Repository(FolderContext context)
        {
            this.context = context;
        }

        public Subfolfers GetSubfolfersByPath(string path)
        {
            return context.Subfolfers.FirstOrDefault(x => x.Directories == path);
        }

        public Files GetFileByPath(string path)
        {
            return context.Files.FirstOrDefault(x => x.files == path);
        }

        public void AddSubfolders(Subfolfers subfolfers)
        {
            context.Subfolfers.Add(subfolfers);
            context.SaveChanges();
        }

        public void AddFiles(Files files)
        {
            context.Files.Add(files);
            context.SaveChanges();
        }

        public void SaveChanges()
        {
            context.SaveChanges();
        }

    }
}

﻿using System;
using System.IO;
using FoldersChecker.DB;
using Serilog;
using System.Collections.Generic;

namespace FoldersChecker
{
    public class Chfiles
    {
        private readonly IRepository repository;
        private readonly ILogger _logger;
        public List<string> FoldersRezult { get { return foldersRezult; } }
        public List<string> FilesRezult { get { return filesRezult; } }
        public List<string> FilesDelete { get { return filesDelete; } }
        public List<string> FilesRemovetoVersion { get { return filesRemovetoVersion; } }
        public int OnliZip { get { return onliZip; } }
        private List<string> filesDelete = new List<string>();
        private List<string> filesRezult = new List<string>();
        private List<string> foldersRezult = new List<string>();
        private List<string> filesRemovetoVersion = new List<string>();
        private int onliZip = 1;

        public Chfiles(IRepository repository, ILogger logger)
        {
            this.repository = repository ?? throw new NullReferenceException(nameof(repository));
            _logger = logger ?? throw new NullReferenceException(nameof(logger));
        }

        public void ClearResults()
        {
            FoldersRezult.Clear();
            FilesRezult.Clear();
            FilesDelete.Clear();
            FilesRemovetoVersion.Clear();
        }

        public string GetComparedPath(string path)
        {
            string comparpachfile;
            var comparedpach = Path.GetDirectoryName(path).Replace("\\.stversions", "");
            var comparedfile = Path.GetFileName(path).Split('~');
            var comparedfilend = Path.GetFileName(path).Split('.');
            if (IsZipFile(path))
                comparpachfile = Path.Combine( comparedpach , comparedfile[0] + "." + comparedfilend[2]);
            else
                comparpachfile = Path.Combine(comparedpach , comparedfile[0] + "." + comparedfilend[1]);
            return comparpachfile;
        }

        public bool IsTickOrHstrFile(string path)
        {
            var extensionPath = Path.GetExtension(path);
            if (extensionPath.Contains(".tck") || extensionPath.Contains(".hstr"))
            { onliZip = onliZip * 0; }
            return extensionPath == ".tck" ^ extensionPath == ".hstr";
        }

        public bool IsZipFile(string path)
        {
            
            var extensionPath = Path.GetExtension(path);
            if (extensionPath.Contains(".zip"))
            { onliZip = onliZip * 0; }
            return extensionPath == ".zip";
        }



        public void CheckFileinFoldrs(string path)
        {
            string newSyncedZipFile = GetComparedPath(path);
            CompareFile FileInfolders = new CompareFile(_logger);
            Console.WriteLine(path + " - " + File.GetLastWriteTime(path) + " - " + File.GetCreationTime(path));

            if (!File.Exists(newSyncedZipFile))
            {
                filesRemovetoVersion.Add(newSyncedZipFile);
            }

            if (Path.GetFileName(path) != null)
            {
                var filechek = repository.GetFileByPath(path);
                var directory = Path.GetDirectoryName(path);
                if (filechek == null)
                {
                    if (repository.GetSubfolfersByPath(directory) == null)
                    {
                        var subfolders = new Subfolfers
                        {
                            Directories = directory,
                            CreationTime = Directory.GetCreationTime(directory).ToString(),
                            LastWriteTime = Directory.GetLastWriteTime(directory).ToString()
                        };
                        repository.AddSubfolders(subfolders);
                        foldersRezult.Add(directory + " ADD folder - " + Directory.GetLastWriteTime(directory).ToString());
                    }

                    var folfer = repository.GetSubfolfersByPath(directory);
                    try
                    {
                        var fileCanBeRemoved = false;
                        if (IsTickOrHstrFile(path))
                        {
                            fileCanBeRemoved = FileInfolders.FileLinesStartWith(File.ReadLines(newSyncedZipFile), File.ReadLines(path));
                        }

                        if (IsZipFile(path))
                        {
                            if (File.Exists(newSyncedZipFile))
                            {
                                fileCanBeRemoved = FileInfolders.ZipArchivesHaveSameContent(path, newSyncedZipFile);
                            }
                        }

                        if (fileCanBeRemoved)
                        {
                            filesDelete.Add(path);
                            _logger.Debug(path + " New data was added at the end of the new file.");
                        }
                        else
                        {
                            var files = new Files
                            {
                                files = path,
                                CreationTime = File.GetCreationTime(path).ToString(),
                                LastWriteTime = File.GetLastWriteTime(path).ToString(),
                                SubfoldersId = folfer.Id
                            };
                            repository.AddFiles(files);
                            foldersRezult.Add(path + " ADD file - " + File.GetLastWriteTime(path).ToString());                           
                        }

                    }
                    catch (Exception ex)
                    {
                        _logger.Error(" - File read error " + ex.Message);
                    }
                }
                else
                {
                    if (filechek.LastWriteTime != File.GetLastWriteTime(path).ToString())
                    {
                        try
                        {
                            var fileCanBeRemoved = false;
                            if (IsTickOrHstrFile(path))
                            {
                                fileCanBeRemoved = FileInfolders.FileLinesStartWith(File.ReadLines(newSyncedZipFile), File.ReadLines(path));
                            }

                            if (IsZipFile(path))
                            {
                                if (File.Exists(newSyncedZipFile))
                                {
                                    fileCanBeRemoved = FileInfolders.ZipArchivesHaveSameContent(path, newSyncedZipFile);
                                }
                            }

                            if (fileCanBeRemoved)
                            {
                                filesDelete.Add(path);
                                _logger.Debug(path + " New data was added at the end of the new file.");
                            }
                            else
                            {
                                filesRezult.Add(path + " - " + File.GetLastWriteTime(path).ToString());
                                filechek.LastWriteTime = File.GetLastWriteTime(path).ToString();
                                repository.SaveChanges();
                            }

                        }
                        catch (Exception ex)
                        {
                            _logger.Error(" - File read error " + ex.Message);
                        }
                    }

                }
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace FoldersChecker.DB
{
    public class FolderContext : DbContext
    {
        public FolderContext(): base("DbConnection")
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<FolderContext, FoldersChecker.Migrations.Configuration>());
        }

        public DbSet<Folders> Folders { get; set; }
        public DbSet<Subfolfers> Subfolfers { get; set; }
        public DbSet<Files> Files { get; set; }
        public DbSet<EmailConfig> EmailConfig { get; set; }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {

        }
    }
}

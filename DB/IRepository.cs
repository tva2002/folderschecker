﻿
namespace FoldersChecker.DB
{
    public interface IRepository
    {
        void AddSubfolders(Subfolfers subfolfers);
        void AddFiles(Files files);
        void SaveChanges();
        Subfolfers GetSubfolfersByPath(string path);
        Files GetFileByPath(string path);
       
    }
}

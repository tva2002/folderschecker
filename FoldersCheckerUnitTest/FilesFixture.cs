﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;

namespace FoldersCheckerUnitTest
{
    public class FilesFixture : IDisposable
    {
        public static readonly string NewFilesDirectory = "C:\\temp\\FoldersCheckerTests";
        public static readonly string VersionsDirectory = Path.Combine(NewFilesDirectory, ".stversions");

        public static readonly string FileWithNewLinesAtTheEndOldVersion = Path.Combine(VersionsDirectory, "TEST_20200504~20200506-085936.tck");
        public static readonly string FileWithNewLinesAtTheEnd = Path.Combine(NewFilesDirectory, "TEST_20200504.tck");
        public static readonly string ChangedFileOldVersion = Path.Combine(VersionsDirectory, "CHANGED_FILE_20200504~20200506-085936.tck");
        public static readonly string ChangedFile = Path.Combine(NewFilesDirectory, "CHANGED_FILE_20200504.tck");
        public static readonly string DeletedFileOldVersion = Path.Combine(VersionsDirectory, "DELETED_FILE_20200504~20200506-085936.tck");

        private static int _instanceCounter = 0;

        public FilesFixture()
        {
            if (_instanceCounter == 0)
            {
                CleanUpTestFilesDirectory();
            }

            Interlocked.Increment(ref _instanceCounter);

            CreateTestDirectoriesAndFiles();
        }

        ~FilesFixture()
        {
            if (_instanceCounter == 0)
            {
                CleanUpTestFilesDirectory();
            }
        }

        public void Dispose()
        {
            GC.Collect(); // Force closing all file streams
            Interlocked.Decrement(ref _instanceCounter);
        }

        private void CreateTestDirectoriesAndFiles()
        {
            if (!Directory.Exists(VersionsDirectory))
            {
                Directory.CreateDirectory(VersionsDirectory);
            }

            File.WriteAllLines(FileWithNewLinesAtTheEndOldVersion, new string[] {
                "ESP35,20200504,060027,6705,6697",
                "ESP35,20200504,060027,6697,6689",
                "ESP35,20200504,060031,6695,6687",
                "ESP35,20200504,060035,6694,6686"
            });

            File.WriteAllLines(FileWithNewLinesAtTheEnd, new string[] {
                "ESP35,20200504,060027,6705,6697",
                "ESP35,20200504,060027,6697,6689",
                "ESP35,20200504,060031,6695,6687",
                "ESP35,20200504,060035,6694,6686",
                "ESP35,20200504,060035,6692,6684",
                "ESP35,20200504,060036,6693,6685"
            });

            File.WriteAllLines(ChangedFileOldVersion, new string[] {
                "ESP35,20200504,060027,6705,6697",
                "ESP35,20200504,060028,6697,6689",
                "ESP35,20200504,060031,6695,6687",
                "ESP35,20200504,060035,6694,6686"
            });

            File.WriteAllLines(ChangedFile, new string[] {
                "ESP35,20200504,060027,6705,6697",
                "ESP35,20200504,060028,6697,6689",
            });

            File.WriteAllLines(DeletedFileOldVersion, new string[] {
                "ESP35,20200504,060036,6693,6685"
            });
        }

        private void CleanUpTestFilesDirectory()
        {
            List<string> FilesForDelete = new List<string>()
            {
                FileWithNewLinesAtTheEndOldVersion,
                FileWithNewLinesAtTheEnd,
                ChangedFileOldVersion,
                ChangedFile,
                DeletedFileOldVersion
            };

            foreach (var file in FilesForDelete)
            {
                TryDeleteFile(file);
            }

            TryDeleteDirectory(VersionsDirectory);
        }

        private void TryDeleteFile(string file)
        {
            if (File.Exists(file))
            {
                try
                {
                    File.Delete(file);
                }
                catch
                {
                }
            }
        }

        private void TryDeleteDirectory(string directory)
        {
            try
            {
                Directory.Delete(directory);
            }
            catch
            {
            }
        }
    }
}

﻿namespace FoldersChecker.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Subfolfers : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Subfolfers",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Directories = c.String(),
                        CreationTime = c.String(),
                        LastWriteTime = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Subfolfers");
        }
    }
}

﻿namespace FoldersChecker.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class changeDBFilesandBSubfolders : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Files", "SubfoldersId", c => c.Int(nullable: false));
            CreateIndex("dbo.Files", "SubfoldersId");
            AddForeignKey("dbo.Files", "SubfoldersId", "dbo.Subfolfers", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Files", "SubfoldersId", "dbo.Subfolfers");
            DropIndex("dbo.Files", new[] { "SubfoldersId" });
            DropColumn("dbo.Files", "SubfoldersId");
        }
    }
}

﻿namespace FoldersChecker.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddNewColumnsToEmailConfigTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.EmailConfigs", "Port", c => c.Int(nullable: false));
            AddColumn("dbo.EmailConfigs", "UserName", c => c.String());
            AddColumn("dbo.EmailConfigs", "Password", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.EmailConfigs", "Password");
            DropColumn("dbo.EmailConfigs", "UserName");
            DropColumn("dbo.EmailConfigs", "Port");
        }
    }
}

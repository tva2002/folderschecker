﻿namespace FoldersChecker.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class files : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Files",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        files = c.String(),
                        CreationTime = c.String(),
                        LastWriteTime = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Folders", "CreationTime", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Folders", "CreationTime");
            DropTable("dbo.Files");
        }
    }
}

﻿namespace FoldersChecker.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class mail : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.EmailConfigs",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        smtpserver = c.String(),
                        from = c.String(),
                        to = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.EmailConfigs");
        }
    }
}

﻿using Serilog;
using System;
using System.Configuration;
using ZabbixSender;

namespace FoldersChecker
{
    class ZabbixAgent
    {
        private readonly ILogger _logger;

        public ZabbixAgent(Serilog.ILogger logger)
        {
            _logger = logger ?? throw new NullReferenceException(nameof(logger));
        }


        public void Send(string msg)
        {
            try
            {
                string zbxServer = ConfigurationManager.AppSettings.Get("ZabbixServer");
                int zbxServerPort = int.Parse(ConfigurationManager.AppSettings.Get("ZabbixServerPort"));
                int zbxServerTimeOut = int.Parse(ConfigurationManager.AppSettings.Get("ZabbixServerTimeOut"));
                string zbxHost = ConfigurationManager.AppSettings.Get("ZabbixHost");
                string zbxKey = ConfigurationManager.AppSettings.Get("ZabbixKey");
                string zbxValue = ConfigurationManager.AppSettings.Get("ZabbixValue");

                ZabbixSenderRequest zabbixAgent = new ZabbixSenderRequest(zbxHost, zbxKey, msg);
                string result = zabbixAgent.Send(zbxServer, zbxServerPort, zbxServerTimeOut).Response;
                if (result == "success")
                {
                    _logger.Debug("Response from Zabbix server is "+ result);
                }
                else
                {
                    _logger.Error("Response from Zabbix server is " + result);
                }
            }
            catch (Exception ex)
            {
                _logger.Error("Zabbix alert send error: {0}", ex);
            }
        }

    }
}

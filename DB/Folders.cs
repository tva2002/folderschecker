﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FoldersChecker.DB
{
    public class Folders
    {
        public int Id { get; set; }
        public string Directories { get; set; }
        public string CreationTime { get; set; }
        public string LastWriteTime { get; set; }

    }
}
